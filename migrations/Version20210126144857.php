<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210126144857 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE traductions ADD groupe_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE traductions ADD CONSTRAINT FK_C3CC68D27A45358C FOREIGN KEY (groupe_id) REFERENCES groupes (id)');
        $this->addSql('CREATE INDEX IDX_C3CC68D27A45358C ON traductions (groupe_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE traductions DROP FOREIGN KEY FK_C3CC68D27A45358C');
        $this->addSql('DROP INDEX IDX_C3CC68D27A45358C ON traductions');
        $this->addSql('ALTER TABLE traductions DROP groupe_id');
    }
}
