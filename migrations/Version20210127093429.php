<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210127093429 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE annotations ADD commentaires VARCHAR(500) DEFAULT NULL, ADD historique JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE etudiants ADD universite VARCHAR(255) DEFAULT NULL, ADD sexe VARCHAR(20) DEFAULT NULL');
        $this->addSql('ALTER TABLE traductions ADD titre1 VARCHAR(255) DEFAULT NULL, ADD titre2 VARCHAR(255) DEFAULT NULL, ADD auteur_article VARCHAR(50) DEFAULT NULL, ADD date_publication DATETIME DEFAULT NULL, ADD source VARCHAR(500) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE annotations DROP commentaires, DROP historique');
        $this->addSql('ALTER TABLE etudiants DROP universite, DROP sexe');
        $this->addSql('ALTER TABLE traductions DROP titre1, DROP titre2, DROP auteur_article, DROP date_publication, DROP source');
    }
}
