<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210127101149 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        // $this->addSql('ALTER TABLE etudiants ADD CONSTRAINT FK_227C02EB2A52F05F FOREIGN KEY (universite_id) REFERENCES universites (id)');
        $this->addSql('CREATE INDEX IDX_227C02EB2A52F05F ON etudiants (universite_id)');
        $this->addSql('ALTER TABLE universites ADD sigle VARCHAR(20) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE etudiants DROP FOREIGN KEY FK_227C02EB2A52F05F');
        $this->addSql('DROP INDEX IDX_227C02EB2A52F05F ON etudiants');
        $this->addSql('ALTER TABLE universites DROP sigle');
    }
}
