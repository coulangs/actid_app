<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210125142306 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE annotations (id INT AUTO_INCREMENT NOT NULL, auteur_id INT NOT NULL, traduction_id INT NOT NULL, date DATETIME DEFAULT NULL, contenu VARCHAR(1000) DEFAULT NULL, type VARCHAR(50) DEFAULT NULL, resolu TINYINT(1) DEFAULT NULL, inner_html VARCHAR(1000) DEFAULT NULL, tags LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_4893180560BB6FE6 (auteur_id), INDEX IDX_489318057E0955EF (traduction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE etudiants (id INT AUTO_INCREMENT NOT NULL, groupe_id INT NOT NULL, etudiant_id INT NOT NULL, alias VARCHAR(50) NOT NULL, langue_maternelle VARCHAR(50) NOT NULL, INDEX IDX_227C02EB7A45358C (groupe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groupes (id INT AUTO_INCREMENT NOT NULL, groupe_id INT NOT NULL, alias VARCHAR(50) NOT NULL, annee INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traductions (id INT AUTO_INCREMENT NOT NULL, date INT NOT NULL, langues VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traductions_etudiants (traductions_id INT NOT NULL, etudiants_id INT NOT NULL, INDEX IDX_3430850D7BA4B99 (traductions_id), INDEX IDX_3430850A873A5C6 (etudiants_id), PRIMARY KEY(traductions_id, etudiants_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE annotations ADD CONSTRAINT FK_4893180560BB6FE6 FOREIGN KEY (auteur_id) REFERENCES etudiants (id)');
        $this->addSql('ALTER TABLE annotations ADD CONSTRAINT FK_489318057E0955EF FOREIGN KEY (traduction_id) REFERENCES traductions (id)');
        $this->addSql('ALTER TABLE etudiants ADD CONSTRAINT FK_227C02EB7A45358C FOREIGN KEY (groupe_id) REFERENCES groupes (id)');
        $this->addSql('ALTER TABLE traductions_etudiants ADD CONSTRAINT FK_3430850D7BA4B99 FOREIGN KEY (traductions_id) REFERENCES traductions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE traductions_etudiants ADD CONSTRAINT FK_3430850A873A5C6 FOREIGN KEY (etudiants_id) REFERENCES etudiants (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE annotations DROP FOREIGN KEY FK_4893180560BB6FE6');
        $this->addSql('ALTER TABLE traductions_etudiants DROP FOREIGN KEY FK_3430850A873A5C6');
        $this->addSql('ALTER TABLE etudiants DROP FOREIGN KEY FK_227C02EB7A45358C');
        $this->addSql('ALTER TABLE annotations DROP FOREIGN KEY FK_489318057E0955EF');
        $this->addSql('ALTER TABLE traductions_etudiants DROP FOREIGN KEY FK_3430850D7BA4B99');
        $this->addSql('DROP TABLE annotations');
        $this->addSql('DROP TABLE etudiants');
        $this->addSql('DROP TABLE groupes');
        $this->addSql('DROP TABLE traductions');
        $this->addSql('DROP TABLE traductions_etudiants');
    }
}
