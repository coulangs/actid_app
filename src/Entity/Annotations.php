<?php

namespace App\Entity;

use App\Repository\AnnotationsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnnotationsRepository::class)
 */
class Annotations
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Etudiants::class, inversedBy="annotations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $auteur;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $contenu;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $resolu;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $innerHtml;

    /**
     * @ORM\ManyToOne(targetEntity=Traductions::class, inversedBy="annotations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $traduction;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $idods;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $commentaires;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $historique = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $tags = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuteur(): ?Etudiants
    {
        return $this->auteur;
    }

    public function setAuteur(?Etudiants $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(?string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getResolu(): ?bool
    {
        return $this->resolu;
    }

    public function setResolu(?bool $resolu): self
    {
        $this->resolu = $resolu;

        return $this;
    }

    public function getInnerHtml(): ?string
    {
        return $this->innerHtml;
    }

    public function setInnerHtml(?string $innerHtml): self
    {
        $this->innerHtml = $innerHtml;

        return $this;
    }

    public function getTraduction(): ?Traductions
    {
        return $this->traduction;
    }

    public function setTraduction(?Traductions $traduction): self
    {
        $this->traduction = $traduction;

        return $this;
    }

    public function getIdods(): ?string
    {
        return $this->idods;
    }

    public function setIdods(string $idods): self
    {
        $this->idods = $idods;

        return $this;
    }

    public function getCommentaires(): ?string
    {
        return $this->commentaires;
    }

    public function setCommentaires(?string $commentaires): self
    {
        $this->commentaires = $commentaires;

        return $this;
    }

    public function getHistorique(): ?array
    {
        return $this->historique;
    }

    public function setHistorique(?array $historique): self
    {
        $this->historique = $historique;

        return $this;
    }

    public function getTags(): ?array
    {
        return $this->tags;
    }

    public function setTags(?array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }
}
