<?php

namespace App\Entity;

use App\Repository\EtudiantsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EtudiantsRepository::class)
 */
class Etudiants
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $alias;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $langueMaternelle;

    /**
     * @ORM\ManyToOne(targetEntity=Groupes::class, inversedBy="etudiants")
     * @ORM\JoinColumn(nullable=false)
     */
    private $groupe;

    /**
     * @ORM\OneToMany(targetEntity=Annotations::class, mappedBy="auteur")
     */
    private $annotations;

    /**
     * @ORM\ManyToMany(targetEntity=Traductions::class, mappedBy="etudiants")
     */
    private $traductions;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $sexe;

    /**
     * @ORM\ManyToOne(targetEntity=Universites::class, inversedBy="etudiants")
     * @ORM\JoinColumn(nullable=true)
     */
    private $universite;

    public function __construct()
    {
        $this->annotations = new ArrayCollection();
        $this->traductions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    public function getLangueMaternelle(): ?string
    {
        return $this->langueMaternelle;
    }

    public function setLangueMaternelle(string $langueMaternelle): self
    {
        $this->langueMaternelle = $langueMaternelle;

        return $this;
    }

    public function getGroupe(): ?Groupes
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupes $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }

    /**
     * @return Collection|Annotations[]
     */
    public function getAnnotations(): Collection
    {
        return $this->annotations;
    }

    public function addAnnotation(Annotations $annotation): self
    {
        if (!$this->annotations->contains($annotation)) {
            $this->annotations[] = $annotation;
            $annotation->setAuteur($this);
        }

        return $this;
    }

    public function removeAnnotation(Annotations $annotation): self
    {
        if ($this->annotations->removeElement($annotation)) {
            // set the owning side to null (unless already changed)
            if ($annotation->getAuteur() === $this) {
                $annotation->setAuteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Traductions[]
     */
    public function getTraductions(): Collection
    {
        return $this->traductions;
    }

    public function addTraduction(Traductions $traduction): self
    {
        if (!$this->traductions->contains($traduction)) {
            $this->traductions[] = $traduction;
            $traduction->addEtudiant($this);
        }

        return $this;
    }

    public function removeTraduction(Traductions $traduction): self
    {
        if ($this->traductions->removeElement($traduction)) {
            $traduction->removeEtudiant($this);
        }

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(?string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getUniversite(): ?Universites
    {
        return $this->universite;
    }

    public function setUniversite(?Universites $universite): self
    {
        $this->universite = $universite;

        return $this;
    }
}
