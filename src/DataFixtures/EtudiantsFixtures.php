<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Etudiants;

class EtudiantsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 1 ; $i <= 3 ; $i++) {
            $etudiant = new Etudiants();

            $etudiant->setAlias("ETU$i")
                    ->setLangueMaternelle('japonais')
                    ->setGroupe();
            
            $manager->persist($etudiant);
        }
        $manager->flush();
    }
}
