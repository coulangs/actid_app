<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Groupes;

class GroupeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 1 ; $i <= 3 ; $i++) {
            $groupe = new Groupes();

            $groupe->setAlias("gtest$i")
                    ->setAnnee('2021');
            
            $manager->persist($groupe);
        }
        $manager->flush();

        $manager->flush();
    }
}
