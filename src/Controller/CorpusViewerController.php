<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Groupes;
use App\Repository\GroupesRepository;
use App\Form\GroupesType;

use App\Entity\Etudiants;
use App\Repository\EtudiantsRepository;
use App\Form\EtuType;

use App\Entity\Traductions;
use App\Repository\TraductionsRepository;
use App\Form\TradType;

class CorpusViewerController extends AbstractController
{

    ///////////////////////////////////////////////////////////
    ///////////////////////// HOME ////////////////////////////
    ///////////////////////////////////////////////////////////

    #[Route('/', name: 'home')]
    public function home(){
        return $this->render('corpus_viewer/index.html.twig');
    }


    ///////////////////////////////////////////////////////////
    //////////////////////// GROUPES //////////////////////////
    ///////////////////////////////////////////////////////////

    #[Route('/groupes', name: 'groupes')]
    public function groupes(GroupesRepository $repo){
        // $article = $repo->find(12);
        // $article = $repo->findOneByTitle('Titre de l\'article');
        // $articles = $repo->findByTitle('Titre de l\'article');
        $groupes = $repo->findAll();

        return $this->render('corpus_viewer/groupes.html.twig', [
            "groupes" => $groupes
        ]);
    }

    #[Route('/groupes/add', name: 'groupesAdd')]
    #[Route('/groupes/{alias}/edit', name: 'groupesEdit')]
    public function form(Groupes $groupe = null, Request $request, EntityManagerInterface $manager){
        if(!$groupe){
            $groupe = new Groupes();
        }

        // $form = $this->createFormBuilder($groupe)
        //             ->add('alias')
        //             ->add('etudiants')
        //             ->add('annee')
        //             ->getForm();

        // méthode encore plus simple en passant par la ligne de commande (créer un formulaire automatique qui s'appellera <nom>Type)
        // commande : php bin/console make:form
        $form = $this->createForm(GroupesType::class, $groupe);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($groupe);
            $manager->flush();
            return $this->redirectToRoute('groupesShow', ['alias' => $groupe->getAlias()]);
        }
        
        return $this->render('corpus_viewer/groupesAdd.html.twig', [
            'formGroupe' => $form->createView(),
            'editMode' => $groupe->getId() !== null,
            'groupe' => $groupe
        ]);
    }


    #[Route('/groupes/{alias}', name: 'groupesShow')]
    public function groupesShow(Groupes $groupe){
        return $this->render('corpus_viewer/groupesShow.html.twig', [
            'groupe' => $groupe
        ]);
    }

    #[Route('/groupes/{alias}/delete', name: 'groupesDelete')]
    public function groupesDelete(Groupes $groupe, EntityManagerInterface $manager){
        $manager->remove($groupe);
        $manager->flush();

        $this->addFlash(
            'success',
            "Le groupe ".$groupe->getAlias()." a bien été supprimée"
        );

        return $this->redirectToRoute('groupes');
    }


    ///////////////////////////////////////////////////////////
    ////////////////////// TRADUCTIONS ////////////////////////
    ///////////////////////////////////////////////////////////

    #[Route('/traductions', name: 'traductions')]
    public function traduction(TraductionsRepository $repo){
        $traductions = $repo->findAll();

        return $this->render('corpus_viewer/traductions.html.twig', [
            "traductions" => $traductions
        ]);
    }

    #[Route('/traductions/add', name: 'traductionsAdd')]
    #[Route('/traductions/{alias}/edit', name: 'traductionsEdit')]
    public function formTrad(Traductions $traduction = null, Request $request, EntityManagerInterface $manager){
        if(!$traduction){
            $traduction = new Traductions();
            $traduction->setDate(date("Y"));
        }

        $form = $this->createForm(TradType::class, $traduction);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($traduction);
            $manager->flush();
            return $this->redirectToRoute('traductionsShow', ['alias' => $traduction->getAlias()]);
        }
        
        return $this->render('corpus_viewer/traductionsAdd.html.twig', [
            'formTrad' => $form->createView(),
            'editMode' => $traduction->getId() !== null,
            'traduction' => $traduction
        ]);
    }

    #[Route('/traductions/{alias}', name: 'traductionsShow')]
    public function traductionsShow(Traductions $traduction){
        
        return $this->render('corpus_viewer/traductionsShow.html.twig', [
            'traduction' => $traduction
        ]);
    }

    #[Route('/traductions/{alias}/delete', name: 'traductionsDelete')]
    public function traductionsDelete(Traductions $traduction, EntityManagerInterface $manager){
        $manager->remove($traduction);
        $manager->flush();

        $this->addFlash(
            'success',
            "La traduction ".$traduction->getAlias()." a bien été supprimée"
        );

        return $this->redirectToRoute('traductions');
    }



    ///////////////////////////////////////////////////////////
    //////////////////////// ETUDIANTS ////////////////////////
    ///////////////////////////////////////////////////////////

    #[Route('/etudiants', name: 'etudiants')]
    public function etudiants(EtudiantsRepository $repo){
        $etudiants = $repo->findAll();

        return $this->render('corpus_viewer/etudiants.html.twig', [
            "etudiants" => $etudiants
        ]);
    }

    #[Route('/etudiants/add', name: 'etudiantsAdd')]
    #[Route('/etudiants/{alias}/edit', name: 'etudiantsEdit')]
    public function formEtu(Etudiants $etudiant = null, Request $request, EntityManagerInterface $manager){
        if(!$etudiant){
            $etudiant = new Etudiants();
        }

        // $form = $this->createFormBuilder($groupe)
        //             ->add('alias')
        //             ->add('etudiants')
        //             ->add('annee')
        //             ->getForm();

        // méthode encore plus simple en passant par la ligne de commande (créer un formulaire automatique qui s'appellera <nom>Type)
        // commande : php bin/console make:form
        $form = $this->createForm(EtuType::class, $etudiant);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($etudiant);
            $manager->flush();
            return $this->redirectToRoute('etudiantsShow', ['alias' => $etudiant->getAlias()]);
        }
        
        return $this->render('corpus_viewer/etudiantsAdd.html.twig', [
            'formEtu' => $form->createView(),
            'editMode' => $etudiant->getId() !== null
        ]);
    }


    #[Route('/etudiants/{alias}', name: 'etudiantsShow')]
    public function etudiantsShow(Etudiants $etudiant){
        return $this->render('corpus_viewer/etudiantsShow.html.twig', [
            'etudiant' => $etudiant
        ]);
    }

    #[Route('/etudiants/{alias}/delete', name: 'etudiantsDelete')]
    public function etudiantsDelete(Etudiants $etudiant, EntityManagerInterface $manager){
        $manager->remove($etudiant);
        $manager->flush();

        $this->addFlash(
            'success',
            "L'étudiant⋅e ".$etudiant->getAlias()." a bien été supprimé⋅e"
        );

        return $this->redirectToRoute('etudiants');
    }




    ///////////////////////////////////////////////////////////
    /////////////////// AFFICHAGE CORPUS///////////////////////
    ///////////////////////////////////////////////////////////

    #[Route('/view/{alias}', name: 'view')]
    public function testActid($alias, TraductionsRepository $repo){

        $traduction = $repo->findBy(array('alias'=>$alias));
        $traduction = $traduction[0];

        $sourceFile = fopen("../public/files/".$alias."_source.xml", 'r') or die("Unable to open file!");
        $initialFile = fopen("../public/files/".$alias."_initial.xml", 'r') or die("Unable to open file!");
        $revisionFile = fopen("../public/files/".$alias."_revision.xml", 'r') or die("Unable to open file!");
        $finalFile = fopen("../public/files/".$alias."_final.xml", 'r') or die("Unable to open file!");
        $infoFile = fopen("../public/files/".$alias."_info.xml", 'r') or die("Unable to open file!");

        $source = fread($sourceFile, filesize("../public/files/".$alias."_source.xml"));
        $initial = fread($initialFile, filesize("../public/files/".$alias."_initial.xml"));
        $revision = fread($revisionFile, filesize("../public/files/".$alias."_revision.xml"));
        $final = fread($finalFile, filesize("../public/files/".$alias."_final.xml"));
        $info = fread($infoFile, filesize("../public/files/".$alias."_info.xml"));

        fclose($sourceFile);
        fclose($initialFile);
        fclose($revisionFile);
        fclose($finalFile);
        fclose($infoFile);

        return $this->render('corpus_viewer/view.html.twig', [
            'xmls' => $source,
            'xmli' => $initial,
            'xmlr' => $revision,
            'xmlf' => $final,
            'xmlinfo' => $info,
            'traduction' => $traduction
        ]);
    }


}
