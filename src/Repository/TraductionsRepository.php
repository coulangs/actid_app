<?php

namespace App\Repository;

use App\Entity\Traductions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Traductions|null find($id, $lockMode = null, $lockVersion = null)
 * @method Traductions|null findOneBy(array $criteria, array $orderBy = null)
 * @method Traductions[]    findAll()
 * @method Traductions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TraductionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Traductions::class);
    }

    // /**
    //  * @return Traductions[] Returns an array of Traductions objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Traductions
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
