<?php

namespace App\Repository;

use App\Entity\Universites;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Universites|null find($id, $lockMode = null, $lockVersion = null)
 * @method Universites|null findOneBy(array $criteria, array $orderBy = null)
 * @method Universites[]    findAll()
 * @method Universites[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UniversitesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Universites::class);
    }

    // /**
    //  * @return Universites[] Returns an array of Universites objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Universites
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
