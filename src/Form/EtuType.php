<?php

namespace App\Form;

use App\Entity\Etudiants;
use App\Entity\Groupes;
use App\Entity\Traductions;
use App\Entity\Universites;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EtuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('alias')
            ->add('langueMaternelle')
            ->add('sexe')
            ->add('groupe', EntityType::class, [
                // looks for choices from this entity
                'class' => Groupes::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'alias',
            
                // used to render a select box, check boxes or radios
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('traductions', EntityType::class, [
                // looks for choices from this entity
                'class' => Traductions::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'alias',
            
                // used to render a select box, check boxes or radios
                'multiple' => true,
                'expanded' => false,
            ])
            ->add('universite', EntityType::class, [
                // looks for choices from this entity
                'class' => Universites::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'nom',
            
                // used to render a select box, check boxes or radios
                'multiple' => false,
                'expanded' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Etudiants::class,
        ]);
    }
}
