<?php

namespace App\Form;

use App\Entity\Groupes;
use App\Entity\Etudiants;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class GroupesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('alias')
            ->add('annee')
            ->add('etudiants', EntityType::class, [
                // looks for choices from this entity
                'class' => Etudiants::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'alias',
            
                // used to render a select box, check boxes or radios
                'multiple' => true,
                'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Groupes::class,
        ]);
    }
}
