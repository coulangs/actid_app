<?php

namespace App\Form;
use App\Entity\Etudiants;
use App\Entity\Groupes;
use App\Entity\Traductions;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class TradType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('alias', TextType::class, [
                'label' => 'Nom de la traduction',
            ])
            ->add('titre1', TextType::class, [
                'label' => 'Titre original',
            ])
            ->add('titre2', TextType::class, [
                'label' => 'Titre traduit',
            ])
            ->add('date', IntegerType::class, [
                'label' => 'Année du groupe',
            ])
            ->add('langues', TextType::class, [
                'label' => 'Code langues',
            ])
            ->add('auteurArticle', TextType::class, [
                'label' => 'Auteur de l\'article',
            ])
            ->add('datePublication', DateType::class, [
                'widget' => 'choice',
                'label' => 'Date de publication de l\'article',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('source')
            ->add('etudiants', EntityType::class, [
                // looks for choices from this entity
                'class' => Etudiants::class,
                'label' => 'Étudiants impliqués',
                
                // uses the User.username property as the visible option string
                'choice_label' => 'alias',
            
                // used to render a select box, check boxes or radios
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('groupe', EntityType::class, [
                // looks for choices from this entity
                'class' => Groupes::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'alias',
            
                // used to render a select box, check boxes or radios
                'multiple' => false,
                'expanded' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Traductions::class,
        ]);
    }
}
