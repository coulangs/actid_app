///////////////////////////////////////
// JAVASCRIPT POUR LA PAGE VIEWER (visualisation du corpus)

// LISTE DES COULEURS POUR DISTINGUER ANNOTATIONS
var editcolor = ['lightblue','orange','yellow','cyan','white','purple','lightblue','orange','yellow','cyan','white','purple'];
//var editcolor = ['lightblue','lightblue','lightblue','lightblue','lightblue','lightblue','lightblue','lightblue'];

// REMPLACEMENT DES BALISES XML → HTML
// <comment> → <span class="commentaire">
xmls = xmls.replace(/<comment/g, '<span class="commentaire"');
xmli = xmli.replace(/<comment/g, '<span class="commentaire"');
xmlr = xmlr.replace(/<comment/g, '<span class="commentaire"');
xmlf = xmlf.replace(/<comment/g, '<span class="commentaire"');

xmls = xmls.replace(/<\/comment>/g, '</span>');
xmli = xmli.replace(/<\/comment>/g, '</span>');
xmlr = xmlr.replace(/<\/comment>/g, '</span>');
xmlf = xmlf.replace(/<\/comment>/g, '</span>');

// <edit type="deleted"> → <span class="suppression">
xmls = xmls.replace(/<edit type="deleted"/g, '<span class="suppression"');
xmli = xmli.replace(/<edit type="deleted"/g, '<span class="suppression"');
xmlr = xmlr.replace(/<edit type="deleted"/g, '<span class="suppression"');
xmlf = xmlf.replace(/<edit type="deleted"/g, '<span class="suppression"');

xmls = xmls.replace(/<\/edit>/g, '</span>');
xmli = xmli.replace(/<\/edit>/g, '</span>');
xmlr = xmlr.replace(/<\/edit>/g, '</span>');
xmlf = xmlf.replace(/<\/edit>/g, '</span>');

// SUIVI DE MODIFICATIONS WORD
// <edit id="ct94606708895024" type="deletion"
xmls = xmls.replace(/<edit type="deletion"/g, '<span class="suiviModif deletion"');
xmli = xmli.replace(/<edit type="deletion"/g, '<span class="suiviModif deletion"');
xmlr = xmlr.replace(/<edit type="deletion"/g, '<span class="suiviModif deletion"');
xmlf = xmlf.replace(/<edit type="deletion"/g, '<span class="suiviModif deletion"');

// <edit id="ct94606708862640" type="insertion"
xmls = xmls.replace(/<edit type="insertion"/g, '<span class="suiviModif insertion"');
xmli = xmli.replace(/<edit type="insertion"/g, '<span class="suiviModif insertion"');
xmlr = xmlr.replace(/<edit type="insertion"/g, '<span class="suiviModif insertion"');
xmlf = xmlf.replace(/<edit type="insertion"/g, '<span class="suiviModif insertion"');

if(window.DOMParser){
    parser = new DOMParser();
    doms = parser.parseFromString(xmls, "text/xml");
    domi = parser.parseFromString(xmli, "text/xml");
    domr = parser.parseFromString(xmlr, "text/xml");
    domf = parser.parseFromString(xmlf, "text/xml");
    dominfo = parser.parseFromString(xmlinfo, "text/xml");
}
console.log("Bonjour.");
document.getElementById('titre').innerHTML = doms.getElementsByTagName('TITRE')[0].innerHTML;

// AFFICHAGES INFOS FICHIER
var idfichier = dominfo.getElementsByTagName('DOCUMENT')[0].id;
document.getElementById('infotraduction').innerHTML = idfichier;

// AFFICHAGE DU NOMBRE TOTAL DE PHRASES
var totalp = doms.getElementsByTagName('P').length;
console.log("Longueur totale :", totalp);
document.getElementById('ptotal').innerHTML = totalp;


// NAVIGATION PHRASE
var currentp = 0;
show(0);

function previous(){
    if(currentp>0){
        currentp -= 1;
        console.log('Previous ',currentp+1, '→', currentp);
        show(currentp);
    }
}

function next(){
    if(currentp<totalp){
        currentp += 1;
        console.log('Next ',currentp-1, '→', currentp);
        show(currentp);
    }
}

// SAUTER DIRECTEMENT A LA PHRASE INDIQUEE
function gotoP(){
    var pid = document.getElementById('pid').value;
    currentp = Number(pid);
    console.log('goTo ',currentp);
    show(currentp);
}
document.getElementById('pid').addEventListener("keyup", function(event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        event.preventDefault(); // Cancel the default action, if needed
        gotoP();
    }
});

// PHRASE SUR UNE SEULE LIGNE
function toggleWordWrap(){
    if (document.getElementById('wordwrap').checked){
        // on décoche
        document.getElementById('maintable').style.width = "100%";
        document.getElementById('wordwrap').checked = false;
        connectAll();
    } else {
        // on coche
        document.getElementById('maintable').style.width = "max-content";
        document.getElementById('wordwrap').checked = true;
        connectAll();
    }
}


// AFFICHER LA PHRASE CIBLE
function show(pid){
    document.getElementById('targs').innerHTML = doms.getElementById('p'+currentp).innerHTML;
    document.getElementById('targi').innerHTML = domi.getElementById('p'+currentp).innerHTML;
    document.getElementById('targr').innerHTML = domr.getElementById('p'+currentp).innerHTML;
    document.getElementById('targf').innerHTML = domf.getElementById('p'+currentp).innerHTML;
    document.getElementById('pid').value = pid;
    document.getElementById('infoparagraphe').innerHTML = doms.getElementById('p'+currentp).parentNode.id;
    document.getElementById('infophrase').innerHTML = 'p'+currentp;

    thisp = domr.getElementById('p'+currentp);

    var listspan = thisp.getElementsByTagName('span');
    document.getElementById('targreditSup').innerHTML = "";
    document.getElementById('targreditInf').innerHTML = "";
    for (span=0; span<listspan.length; span++){
        if (listspan[span].classList != "suppression"){
            newEdit = document.createElement('div');
            newEdit.classList = "divEdit align-self-center";
            newEdit.id = 'div'+listspan[span].id;

            newEditTitle = document.createElement('div');
            newEditTitle.classList = "divEditTitle";
            newEditTitle.innerHTML = listspan[span].getAttribute("aut");
            newEdit.appendChild(newEditTitle);

            newEditDate = document.createElement('div');
            newEditDate.classList = "divEditDate";
            newEditDate.innerHTML = listspan[span].getAttribute("date");
            newEdit.appendChild(newEditDate);

            if (listspan[span].getAttribute("first-type")){
                // Cas du suivi de modif, suppression d'un ajout
                newEditFirstInsert = document.createElement('div');
                newEditFirstInsert.classList = "divEditFirstInsert";
                newEditFirstInsert.innerHTML = "inséré par " + listspan[span].getAttribute("first-aut") + "<br>" + listspan[span].getAttribute("first-date");
                newEdit.appendChild(newEditFirstInsert);
            } else {
                // Sinon on affiche simplement la valeur de text
                newEditText = document.createElement('div');
                newEditText.classList = "divEditText";
                newEditText.innerHTML = listspan[span].getAttribute("text");
                newEdit.appendChild(newEditText);
            }

            newEditId = document.createElement('div');
            newEditId.classList = "divEditId";
            newEditId.innerHTML = "id:"+listspan[span].getAttribute("id");
            newEdit.appendChild(newEditId);

            if (listspan[span].classList == "suiviModif deletion"){
                document.getElementById('targreditSup').appendChild(newEdit);
            } else {
                document.getElementById('targreditInf').appendChild(newEdit);
            }

            // Colorations
            if (listspan[span].classList == "suiviModif insertion"){
                document.getElementById('div'+listspan[span].id).style.borderColor = "green";
            } else if (listspan[span].classList == "suiviModif deletion"){
                document.getElementById('div'+listspan[span].id).style.borderColor = "red";
            } else {
                document.getElementById(listspan[span].id).style.borderColor = editcolor[span];
                document.getElementById('div'+listspan[span].id).style.borderColor = editcolor[span];
            }
        }
    }
    connectAll();


}
