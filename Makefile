install:
	cp .env.test .env
	vim .env
	composer update
	composer install
	npm install --force
	npm run dev
	php bin/console doctrine:database:create --if-not-exists
	php bin/console doctrine:schema:update

update:
	composer update
	composer install
	npm install --force
	npm run dev
	php bin/console doctrine:schema:update
	php bin/console cache:clear
	php bin/console cache:warmup


install-prod:
	cp .env.test .env
	vim .env
	composer update
	composer install --no-dev --optimize-autoloader
	npm install
	npm run build
	php bin/console doctrine:database:create --if-not-exists
	php bin/console doctrine:schema:update --force
	php bin/console cache:clear --env=prod
	php bin/console cache:warmup --env=prod

update-prod:
	composer install
	npm run build
	php bin/console doctrine:schema:update
	php bin/console cache:clear --env=prod
	php bin/console cache:warmup --env=prod

clear-cache:
	php bin/console cache:clear --env=prod
	php bin/console cache:warmup --env=prod