
window.alert('Attention assets/js/viewer.js est exécuté !');
// LISTE DES COULEURS POUR DISTINGUER ANNOTATIONS
var editcolor = ['lightblue','orange','yellow','cyan','white','purple','lightblue','orange','yellow','cyan','white','purple'];
//var editcolor = ['lightblue','lightblue','lightblue','lightblue','lightblue','lightblue','lightblue','lightblue'];

// REMPLACEMENT DES BALISES XML → HTML
// <comment> → <span class="commentaire">
xmls = xmls.replace(/<comment/g, '<span class="commentaire"');
xmli = xmli.replace(/<comment/g, '<span class="commentaire"');
xmlr = xmlr.replace(/<comment/g, '<span class="commentaire"');
xmlf = xmlf.replace(/<comment/g, '<span class="commentaire"');

xmls = xmls.replace(/<\/comment>/g, '</span>');
xmli = xmli.replace(/<\/comment>/g, '</span>');
xmlr = xmlr.replace(/<\/comment>/g, '</span>');
xmlf = xmlf.replace(/<\/comment>/g, '</span>');

// <edit type="deleted"> → <span class="suppression">
xmls = xmls.replace(/<edit type="deleted"/g, '<span class="suppression"');
xmli = xmli.replace(/<edit type="deleted"/g, '<span class="suppression"');
xmlr = xmlr.replace(/<edit type="deleted"/g, '<span class="suppression"');
xmlf = xmlf.replace(/<edit type="deleted"/g, '<span class="suppression"');

xmls = xmls.replace(/<\/edit>/g, '</span>');
xmli = xmli.replace(/<\/edit>/g, '</span>');
xmlr = xmlr.replace(/<\/edit>/g, '</span>');
xmlf = xmlf.replace(/<\/edit>/g, '</span>');

// SUIVI DE MODIFICATIONS WORD
// <edit id="ct94606708895024" type="deletion"
xmls = xmls.replace(/<edit type="deletion"/g, '<span class="suiviModif deletion"');
xmli = xmli.replace(/<edit type="deletion"/g, '<span class="suiviModif deletion"');
xmlr = xmlr.replace(/<edit type="deletion"/g, '<span class="suiviModif deletion"');
xmlf = xmlf.replace(/<edit type="deletion"/g, '<span class="suiviModif deletion"');

// <edit id="ct94606708862640" type="insertion"
xmls = xmls.replace(/<edit type="insertion"/g, '<span class="suiviModif insertion"');
xmli = xmli.replace(/<edit type="insertion"/g, '<span class="suiviModif insertion"');
xmlr = xmlr.replace(/<edit type="insertion"/g, '<span class="suiviModif insertion"');
xmlf = xmlf.replace(/<edit type="insertion"/g, '<span class="suiviModif insertion"');

if(window.DOMParser){
    parser = new DOMParser();
    doms = parser.parseFromString(xmls, "text/xml");
    domi = parser.parseFromString(xmli, "text/xml");
    domr = parser.parseFromString(xmlr, "text/xml");
    domf = parser.parseFromString(xmlf, "text/xml");
    dominfo = parser.parseFromString(xmlinfo, "text/xml");
}
console.log("Bonjour.");
document.getElementById('titre').innerHTML = doms.getElementsByTagName('TITRE')[0].innerHTML;

// AFFICHAGES INFOS FICHIER
var idfichier = dominfo.getElementsByTagName('DOCUMENT')[0].id;
document.getElementById('infotraduction').innerHTML = idfichier;

// AFFICHAGE DU NOMBRE TOTAL DE PHRASES
var totalp = doms.getElementsByTagName('P').length;
console.log("Longueur totale :", totalp);
document.getElementById('ptotal').innerHTML = totalp;


// NAVIGATION PHRASE
var currentp = 0;
show(0);

function previous(){
    if(currentp>0){
        currentp -= 1;
        console.log('Previous ',currentp+1, '→', currentp);
        show(currentp);
    }
}

function next(){
    if(currentp<totalp){
        currentp += 1;
        console.log('Next ',currentp-1, '→', currentp);
        show(currentp);
    }
}

// SAUTER DIRECTEMENT A LA PHRASE INDIQUEE
function gotoP(){
    var pid = document.getElementById('pid').value;
    currentp = Number(pid);
    console.log('goTo ',currentp);
    show(currentp);
}
document.getElementById('pid').addEventListener("keyup", function(event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        event.preventDefault(); // Cancel the default action, if needed
        gotoP();
    }
});

// PHRASE SUR UNE SEULE LIGNE
function toggleWordWrap(){
    if (document.getElementById('wordwrap').checked){
        // on décoche
        document.getElementById('maintable').style.width = "100%";
        document.getElementById('wordwrap').checked = false;
        connectAll();
    } else {
        // on coche
        document.getElementById('maintable').style.width = "max-content";
        document.getElementById('wordwrap').checked = true;
        connectAll();
    }
}


// AFFICHER LA PHRASE CIBLE
function show(pid){
    document.getElementById('targs').innerHTML = doms.getElementById('p'+currentp).innerHTML;
    document.getElementById('targi').innerHTML = domi.getElementById('p'+currentp).innerHTML;
    document.getElementById('targr').innerHTML = domr.getElementById('p'+currentp).innerHTML;
    document.getElementById('targf').innerHTML = domf.getElementById('p'+currentp).innerHTML;
    document.getElementById('pid').value = pid;
    document.getElementById('infoparagraphe').innerHTML = doms.getElementById('p'+currentp).parentNode.id;
    document.getElementById('infophrase').innerHTML = 'p'+currentp;

    thisp = domr.getElementById('p'+currentp);

    var listspan = thisp.getElementsByTagName('span');
    document.getElementById('targreditSup').innerHTML = "";
    document.getElementById('targreditInf').innerHTML = "";
    for (span=0; span<listspan.length; span++){
        if (listspan[span].classList != "suppression"){
            newEdit = document.createElement('div');
            newEdit.classList = "divEdit align-self-center";
            newEdit.id = 'div'+listspan[span].id;

            newEditTitle = document.createElement('div');
            newEditTitle.classList = "divEditTitle";
            newEditTitle.innerHTML = listspan[span].getAttribute("aut");
            newEdit.appendChild(newEditTitle);

            newEditDate = document.createElement('div');
            newEditDate.classList = "divEditDate";
            newEditDate.innerHTML = listspan[span].getAttribute("date");
            newEdit.appendChild(newEditDate);

            if (listspan[span].getAttribute("first-type")){
                // Cas du suivi de modif, suppression d'un ajout
                newEditFirstInsert = document.createElement('div');
                newEditFirstInsert.classList = "divEditFirstInsert";
                newEditFirstInsert.innerHTML = "inséré par " + listspan[span].getAttribute("first-aut") + "<br>" + listspan[span].getAttribute("first-date");
                newEdit.appendChild(newEditFirstInsert);
            } else {
                // Sinon on affiche simplement la valeur de text
                newEditText = document.createElement('div');
                newEditText.classList = "divEditText";
                newEditText.innerHTML = listspan[span].getAttribute("text");
                newEdit.appendChild(newEditText);
            }

            newEditId = document.createElement('div');
            newEditId.classList = "divEditId";
            newEditId.innerHTML = "id:"+listspan[span].getAttribute("id");
            newEdit.appendChild(newEditId);

            if (listspan[span].classList == "suiviModif deletion"){
                document.getElementById('targreditSup').appendChild(newEdit);
            } else {
                document.getElementById('targreditInf').appendChild(newEdit);
            }

            // Colorations
            if (listspan[span].classList == "suiviModif insertion"){
                document.getElementById('div'+listspan[span].id).style.borderColor = "green";
            } else if (listspan[span].classList == "suiviModif deletion"){
                document.getElementById('div'+listspan[span].id).style.borderColor = "red";
            } else {
                document.getElementById(listspan[span].id).style.borderColor = editcolor[span];
                document.getElementById('div'+listspan[span].id).style.borderColor = editcolor[span];
            }
        }
    }
    connectAll();


}


///////////////////////////////////////
// CODE POUR L'AFFICHAGE DES PATHS SVG
//https://gist.github.com/alojzije/11127839#file-style-css-L14
//helper functions, it turned out chrome doesn't support Math.sgn() 
function signum(x) {
return (x < 0) ? -1 : 1;
}
function absolute(x) {
return (x < 0) ? -x : x;
}

function drawPath(svg, path, startX, startY, endX, endY) {
// get the path's stroke width (if one wanted to be  really precize, one could use half the stroke size)
var stroke =  parseFloat(path.attr("stroke-width"));
// check if the svg is big enough to draw the path, if not, set heigh/width
if (svg.attr("height") <  endY)                 svg.attr("height", endY);
if (svg.attr("width" ) < (startX + stroke) )    svg.attr("width", (startX + stroke));
if (svg.attr("width" ) < (endX   + stroke) )    svg.attr("width", (endX   + stroke));

var deltaX = (endX - startX) * 0.15;
var deltaY = (endY - startY) * 0.15;
// for further calculations which ever is the shortest distance
var delta  =  deltaY < absolute(deltaX) ? deltaY : absolute(deltaX);

// set sweep-flag (counter/clock-wise)
// if start element is closer to the left edge,
// draw the first arc counter-clockwise, and the second one clock-wise
var arc1 = 0; var arc2 = 1;
if (startX > endX) {
    arc1 = 1;
    arc2 = 0;
}
// draw tha pipe-like path
// 1. move a bit down, 2. arch,  3. move a bit to the right, 4.arch, 5. move down to the end 
path.attr("d",  "M"  + startX + " " + startY +
                " V" + (startY + delta) +
                " A" + delta + " " +  delta + " 0 0 " + arc1 + " " + (startX + delta*signum(deltaX)) + " " + (startY + 2*delta) +
                " H" + (endX - delta*signum(deltaX)) + 
                " A" + delta + " " +  delta + " 0 0 " + arc2 + " " + endX + " " + (startY + 3*delta) +
                " V" + endY );
}

function connectElements(svg, path, startElem, endElem) {
var svgContainer= $("#svgContainer");

// if first element is lower than the second, swap!
if(startElem.offset().top > endElem.offset().top){
    var temp = startElem;
    startElem = endElem;
    endElem = temp;
}

// get (top, left) corner coordinates of the svg container   
var svgTop  = svgContainer.offset().top;
var svgLeft = svgContainer.offset().left;

// get (top, left) coordinates for the two elements
var startCoord = startElem.offset();
var endCoord   = endElem.offset();

// calculate path's start (x,y)  coords
// we want the x coordinate to visually result in the element's mid point
var startX = startCoord.left + 0.5*startElem.outerWidth() - svgLeft;    // x = left offset + 0.5*width - svg's left offset
var startY = startCoord.top  + startElem.outerHeight() - svgTop;        // y = top offset + height - svg's top offset

    // calculate path's end (x,y) coords
var endX = endCoord.left + 0.5*endElem.outerWidth() - svgLeft;
var endY = endCoord.top  - svgTop;

// call function for drawing the path
drawPath(svg, path, startX, startY, endX, endY);

}



function connectAll() {
// Réinitialisation
document.getElementById('svg1').innerHTML = '<path id="myNewPath" d="M0 0" stroke-width="3px" style="stroke:#555; fill:none;  "></path>';

// Commentaires
var listcommentaires = document.getElementsByClassName('commentaire');
for (i=0; i<listcommentaires.length; i++){
    var thisid = listcommentaires[i].id;
    var thiscolor = document.getElementById(thisid).style.borderColor;
    var newPath = document.createElementNS("http://www.w3.org/2000/svg", "path");
    newPath.setAttributeNS(null, 'id',"path"+thisid);
    newPath.setAttributeNS(null, 'd', "M0 0");
    newPath.setAttributeNS(null, 'stroke-width', '1px');
    newPath.setAttributeNS(null, 'style', 'stroke:'+thiscolor+'; fill:none;');
    document.getElementById('svg1').appendChild(newPath);
    
    connectElements($("#svg1"), $("#path"+thisid), $("#"+thisid),  $("#div"+thisid));
}

// Suivis de modifications
var listsuiviModif = document.getElementsByClassName('suiviModif');
for (i=0; i<listsuiviModif.length; i++){
    var thisid = listsuiviModif[i].id;
    var insertion = false;
    var thiscolor = "red";
    if (document.getElementById(thisid).classList == 'suiviModif insertion'){
        insertion = true;
        thiscolor = "green";
    }
    var newPath = document.createElementNS("http://www.w3.org/2000/svg", "path");
    newPath.setAttributeNS(null, 'id',"path"+thisid);
    newPath.setAttributeNS(null, 'd', "M0 0");
    newPath.setAttributeNS(null, 'stroke-width', '1px');
    newPath.setAttributeNS(null, 'style', 'stroke:'+thiscolor+'; fill:none;');
    document.getElementById('svg1').appendChild(newPath);
    
    connectElements($("#svg1"), $("#path"+thisid), $("#"+thisid),  $("#div"+thisid));
}

}

$(document).ready(function() {
// reset svg each time 
$("#svg1").attr("height", "0");
$("#svg1").attr("width", "0");
connectAll();
});

$(window).resize(function () {
// reset svg each time 
$("#svg1").attr("height", "0");
$("#svg1").attr("width", "0");
connectAll();
});
